package com.wall.student.namnguyen;

import java.util.Scanner;


public class Bai1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Nhập số : ");
        int n = s.nextInt();
        if (SNT(n)) {
            System.out.println(n + " Là số nguyên tố");
        } else {
            System.out.println(n + " không là số nguyên tố");

        }
    }

    public static boolean SNT(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}