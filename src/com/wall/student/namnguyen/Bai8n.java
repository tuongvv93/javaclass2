public class Bai8n {


        static int[] a = new int[]{2, 3, 5, 7};
        static int[] so7ChuSo = new int[7];
        static int n = 0;
        public static void main(String[] args) {
            deQuy(0, 0);

        }

        static void deQuy(int oldI, int vt) {
            if (vt == 7) {
                int number = convertArrToNumber(so7ChuSo);
                if (isNguyenTo(number)) {
                    System.out.println(number);
                }
                return;
            }
            for (int i = oldI; i < a.length; i++) {
                so7ChuSo[vt] = a[i];
                deQuy(i, vt + 1);
            }
        }

         static int convertArrToNumber(int[] so7ChuSo) {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < 7; i++) {
                s = s.append(so7ChuSo[i]);
            }
            return Integer.parseInt(s.toString());
        }

         public static boolean isNguyenTo(int n) {
            if (n < 2) return false;
            if (n == 2) return true;
            int canBacHai = (int) Math.sqrt(n);
            for (int i = 3; i <= canBacHai; i += 2) {
                if (n % i == 0) return false;
            }
            return true;
        }
    }



    // cách cơ bản phần c
//    int[] a = new int[]{2, 3, 5, 7};
//        for (int i1 = 0; i1 < 4; i1++) {
//            for (int i2 = i1; i2 < 4; i2++) {
//                for (int i3 = i2; i3 < 4; i3++) {
//                    for (int i4 = i3; i4 < 4; i4++) {
//                        for (int i5 = i4; i5 < 4; i5++) {
//                            for (int i6 = i5; i6 < 4; i6++) {
//                                for (int i7 = i6; i7 < 4; i7++) {
//



//                                    int so7ChuSo = Integer.parseInt(String.valueOf("" + a[i1] + a[i2] + a[i3] + a[i4] + a[i5] + a[i6] + a[i7]));
//                                    //System.out.println(so7ChuSo);
//                                    if (isNguyenTo(so7ChuSo)) {
//                                        System.out.println(so7ChuSo);
//                                    }
//                                }
//                            }
//                        }
//                     }
//              }
//}
//      }

