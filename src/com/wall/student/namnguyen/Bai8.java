public class Bai8 {
    public static boolean checkSNT(int n){
        if(n>1){
            for(int i=2;i<=Math.sqrt(n);i++){
                if(n%i==0)
                    return false;
            }
            return true;
        }
        else
            return false;
    }
    public static boolean nguyenTo(int n){
        while(n!=0){
            if(!checkSNT(n%10)) return false;
            n/= 10;
        }
        return true;
    }


    public static void main(String[] args) {
        int i,count= 0;
        System.out.println("các số có 7 chữ số thoả mãn điều kiện là: ");
        for(i=1000000 ; i<9999999 ; i++){
            if(checkSNT(i) && nguyenTo(i)){
                System.out.println(" "+i); count++;
            }
        }
        System.out.println("\n Có "+count+" số thỏa mãn");
    }
}

