package com.wall.baitap.student.nmnhat;
import java.util.Scanner;
public class bai7m2 {
    public static void maxday(int a[], int n){
        int max1=0;int max2=0;
        for(int i=1;i<=n;i++){
            if(a[i]>max1) {
                max2=max1;
                max1=a[i];
            }
            else if(a[i]>max2) max2=a[i];
        }
        System.out.println("gia tri lon nhat day so la: "+max1);
        System.out.println("gia tri lon thu 2 day so la: "+max2);
    }

    public static void chenday(int []a,int x, int n){
        n++; a[n]=x;
        sapxep(a, n);
        System.out.println("day sau khi chen la: ");
        for(int i=1;i<=n;i++) System.out.print("  "+a[i]);
    }

    public static void sapxep(int []a, int n){
        int tg;
        for(int i=1;i<n;i++)
            for(int j=i+1;j<=n;j++)
                if(a[i]<a[j]){
                    tg=a[i];
                    a[i]=a[j];
                    a[j]=tg;
                }

    }

    public static void main(String[] args) {
        int n, x;
        int a[]=new int[100];
        Scanner in= new Scanner(System.in);
        System.out.print("nhap so phan tu cua mang: "); n=in.nextInt();
        System.out.println("nhap cac phan tu cua mang: ");
        for(int i=1;i<=n;i++){
            System.out.print("a["+i+"]= "); a[i]= in.nextInt();
        }
        maxday(a,n);
        sapxep(a,n);
        System.out.println("day sau khi sap xep giam dan la: ");
        for(int i=1;i<=n;i++) System.out.print("  "+a[i]);
        System.out.print("\n nhap so can chen: "); x=in.nextInt();
        chenday(a,x,n);
        System.out.println("");
    }
}
