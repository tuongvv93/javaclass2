package javaclass2.src.com.wall.student.nmnhat.cua;

public abstract class Cua {
    public int getDong() {
        return dong;
    }

    public void setDong(int dong) {
        this.dong = dong;
    }

    public int getMo() {

        return mo;
    }

    public void setMo(int mo) {

        this.mo = mo;
    }

    private int dong;
    private int mo;

    public abstract void kieuDong();

    public void cachDong(){
        kieuDong();
        System.out.println("Ok");
    }
}
