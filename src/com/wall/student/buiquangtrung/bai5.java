package com.wall.student.buiquangtrung;
//Liet ke cac so thuan nghich 6 chu so  (10^5)
public class bai5 {
    public static boolean isThuanNghich(int n) {
        String str = String.valueOf(n);
        int size = str.length();
        for (int i = 0; i < (size/2); i++) {
            if (str.charAt(i) != str.charAt(size - i - 1)) {
                return false;//khong thuan nghich
            }
        }
        return true;//thuan nghich
    }

    public static void main(String[] args) {
        int count = 0;
        // in cac so thu duoc 6 chu so
        for (int i = 100000; i < 1000000; i++) {
            if (isThuanNghich(i)) {
                System.out.println(i);
                count++;
            }
        }
        System.out.println("Tổng các số thuận nghịch có 6 chữ số: "
                + count);
    }
}
