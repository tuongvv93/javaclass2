package com.wall.student.buiquangtrung;
//ktra uoc chung lon nhat 2 so a va b
import java.util.Scanner;

public class bai2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap a: ");
        int a = sc.nextInt();
        System.out.println("Nhap b: ");
        int b = sc.nextInt();
        int min;
        int ucln = 0;


        if (a == 0 && b == 0) {
            System.out.println("KO co UCLN");
        } else if (a == 0 && b != 0) {
            System.out.println(b + " La UCLN");
        } else if (a != 0 && b == 0) {
            System.out.println(a + " La UCLN");
        } else {
            if (a > b) {
                min = b;
            } else {
                min = a;
            }
            for (int i = 1; i <= min; i++) {
                if (a % i == 0 && b % i == 0) {
                    ucln = i;
                }
            }
            System.out.println("UCLN la " + ucln);
        }
    }
}

