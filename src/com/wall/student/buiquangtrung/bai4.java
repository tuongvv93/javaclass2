package com.wall.student.buiquangtrung;

import java.util.Scanner;

public class bai4 {
    public static boolean SNT(int n) {
        // so nguyen n < 2 khong phai la so nguyen to
        if (n < 2) return false;
        // check so nguyen to khi n >= 2
        int canB2 = (int) Math.sqrt(n);
        for (int i = 2; i <= canB2; i++)  if (n % i == 0)  return false;
        return true;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập n = ");
        int n = scanner.nextInt();
        System.out.printf("Trong %d so , nhung so nguyen to la : \n", n);
        int dem = 0;
        int i = 2;
        while (dem < n) {
            if (SNT(i)) {
                System.out.print(i + " ");
                dem++;
            }
            i++;
        }
    }
}
