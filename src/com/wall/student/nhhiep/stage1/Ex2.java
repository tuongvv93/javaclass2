package com.wall.student.nhhiep.stage1;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số nguyên dương a = ");
        int a = sc.nextInt();
        System.out.print("Nhập số nguyên dương b = ");
        int b = sc.nextInt();
        System.out.println("USCLN của " + a + " và " + b + " là: " + USCLN(a, b));
    }

    public static int USCLN(int a, int b) {
        if (b == 0)
            return a;
        return USCLN(b, a % b);
    }

}
