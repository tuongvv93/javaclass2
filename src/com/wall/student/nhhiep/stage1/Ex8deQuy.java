package com.wall.java1.stage1;

public class Ex8deQuy {
    static int[] a = new int[]{2, 3, 5, 7};
    static int n = 0;

    public static void main(String[] args) {
        deQuy(0);
    }

    static void deQuy(int vt) {
        if (vt == 7) {
            if (isPrime(n)){
                System.out.println(n);
            }

            return;
        }

        for (int i = 0; i < a.length; i++) {
            int x = n;
//            n = ((n+1) * 10) + a[i];
            n = (int)((Math.pow(1,i)) * n) + a[i];

            System.out.println("test"+ n);
            deQuy(vt+1);

        }
    }
    public static boolean isPrime(int n) {
        if (n < 2) return false;
        int sqrtn = (int) Math.sqrt(n);
        for (int i = 2; i <= sqrtn; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }
}
