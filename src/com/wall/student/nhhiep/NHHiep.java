package com.wall.student.nhhiep;
import java.util.Scanner;

public class NHHiep {
    static void myMethod(){
        double a;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập a: ");
        a = sc.nextDouble();
        double x = Math.sqrt(a);
        int x2 = (int)x;
        if(x==x2){
            // chứa nhiều dòng code
            System.out.println("là số chính phương");
        } else {
            System.out.println("không là số cp");
        }
    }

    public static void main(String[] args) {
        //System.out.println("Hello World");
        //algorithm
        //Kiểu dữ liệu: số nguyên -> int
        //              số thập phân -> float
        //              logic -> bool
        //              double lớn hơn float

        //Kiểm tra số chính phương
        myMethod();
    }
}
