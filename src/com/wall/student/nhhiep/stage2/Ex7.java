package com.wall.student.nhhiep.stage2;

import java.util.Scanner;

public class Ex7 {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập n: ");
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập phần tử thứ " + (i+1) + " :");
            a[i] = sc.nextInt();
        }
        timMax(a);
        sapXep(a);


    }
    public static void timMax(int a[]) {
        int max = a[0];
        int max2 = 0;
        for (int i = 1; i < a.length; i++) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        for (int i = 0; i < a.length; i++) {
            if (max == a[i]) {
                continue;
            } else if (max2 < a[i]) {
                max2 = a[i];
            }
        }
        System.out.println("Phần tử lớn nhất là: " + max);
        System.out.println("Phần tử lớn thứ 2 là: " + max2);
    }

    public static void sapXep(int a[]) {
        int temp = a[0];
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] < a[j]) {
                    temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }

        }show(a);
    }

    public static void show(int [] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
