package com.wall.java1;

import java.util.Scanner;

public class BaiB {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập n");
        double n = sc.nextDouble();
        double min = Math.pow(10, n - 1);
        double max = Math.pow(10, n);
        int count = 0;
        for (double i = min; i < max; i++) {
            if (checkDK(n) && checkChiaHet10(n)) {
                count++;
            }
        }
        System.out.println(count);
    }

    public static boolean checkDK(double n) {
        StringBuilder str = new StringBuilder();
        int checkTN = Integer.parseInt(str.reverse().toString());
        if (n == checkTN) {
            if (n % 10 == 0) return true;
        }
        return false;
    }

    public static boolean checkChiaHet10(double n) {
        int tong = 0;
        while (n > 0) {
            int t = (int) n % 10;
            tong = tong + t;
            n = n / 10;
        }
        if (tong % 10 == 0) {
            return true;
        }
        return false;
    }
}

