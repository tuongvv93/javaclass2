package com.wall.java1;

import java.util.Scanner;

public class BaiN {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập số bộ test t: ");
        int t = sc.nextInt();
        System.out.println("Nhập số cửa hàng: ");
        int nch = sc.nextInt();
        System.out.println("Nhập toạ độ các cửa hàng");
        int[] ntd = new int[nch];
        for (int i = 0; i < nch; i++) {
            ntd[i] = sc.nextInt();
        }
        System.out.println(minMax(ntd));
    }

    public static int minMax(int[] ntd) {
        int min = ntd[0];
        int max = ntd[0];
        for (int i = 0; i < ntd.length; i++) {
            if(ntd[i]<min){
                min = ntd[i];
            }
            if(ntd[i]>max){
                max = ntd[i];
            }
        }
        int minDistance = (max-min)*2;
        return minDistance;
    }

}
