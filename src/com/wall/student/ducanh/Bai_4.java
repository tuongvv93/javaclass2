package com.wall.student.ducanh;

import java.util.Scanner;

public class Bai_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào 1 số: ");
        int num = sc.nextInt();
        for (int i = 1; i <= num; i++) {
            int a = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    a += 1;
                }
            }
            if (a == 2) {
                System.out.print(i + " ");
            }
        }
    }
}