package com.wall.student.ducanh;

import java.util.Scanner;

public class ViDu1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap vao 1 so: ");
        double num = sc.nextDouble();
        double sn = Math.sqrt(num);
        int a = (int) sn;
        if ((a - sn) == 0) {
            System.out.println(num + " Là số chính phương");
        } else {
            System.out.println(num + " Ko phải số chính phương");
        }
    }
}

