package com.wall.student.doanduc;

import java.util.Scanner;

public class Bai_2 {
    /*Viết chương trình tìm ước số chung lớn nhất của hai số nguyên dương a,b nhập từ bàn phím*/
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào số thứ nhất: ");
        int a = sc.nextInt();
        System.out.println("Nhập vào số thứ hai: ");
        int b = sc.nextInt();
        int min;
        int ucln = 0;
        //TH1: 2 số đều bằng 0
        if (a == 0 && b == 0) {
            System.out.println("KO có UCLN");
        } else if (a == 0 && b != 0) {  //TH2: 1 trg 2 số = 0
            System.out.println(b + " Là UCLN");
        } else if (a != 0 && b == 0) {  //TH2: 1 trg 2 số = 0
            System.out.println(a + " Là UCLN");
        } else {
            if (a > b) {
                min = b;
            } else {
                min = a;
            }
            for (int i = 1; i <= min; i++) {
                if (a % i == 0 && b % i == 0) {
                    ucln = i;
                }
            }
            System.out.println("UCLN là " + ucln);
        }
    }
}
