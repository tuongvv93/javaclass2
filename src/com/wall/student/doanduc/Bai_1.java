package com.wall.student.doanduc;

import java.util.Scanner;

public class Bai_1 {
    //Viết chương trình nhập số nguyên n và kiểm tra n có phải số nguyên tố hay không
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào 1 số: ");
        int n = sc.nextInt();
        int a = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                a += 1;
            }
        }
        if (a == 2) {
            System.out.println(n + " Là số nguyên tố");
        } else {
            System.out.println(n + " Ko là số nguyên tố");
        }

    }
}
