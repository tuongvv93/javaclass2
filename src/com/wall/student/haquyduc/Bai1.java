package com.wall.student.haquyduc;

import java.util.Scanner;

// format code Crl + ALT +L
public class Bai1 {
    public static void main(String[] args) {
        System.out.println("Nhập số nguyên :");
        int n = CheckInput();
        if (SNT(n)) {
            System.out.println(n + " Là số nguyên tố!");
        } else {
            System.out.println(n + " Không là số nguyên tố!");
        }
    }

    public static int CheckInput() {
        int n = 0;
        boolean check = false;
        Scanner scanner = new Scanner(System.in);
        while (!check) {
            try {
                n = scanner.nextInt();
                check = true;
            } catch (Exception e) {
                System.out.println("Xin mời nhập vào số nguyên");
                scanner.nextLine();
            }
        }
        return n;
    }

    public static boolean SNT(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
