package com.wall.student.haquyduc;


import java.util.Scanner;

public class BaiSodep {
    public static void main(String[] args) {
        System.out.print("Nhập n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int dem = 0;
        for (int i = (int) Math.pow(0, n - 1); i < Math.pow(10, n); i++)
            if (isThuanNghich(i) && tongchia10(i))
                System.out.println(i+"");
                dem++;
        System.out.println("dem :"+n);

    }

    public static boolean tongchia10(int n) {
        int tong = 0;
        while (n > 0) {
            int t = n % 10;
            tong = tong + t;
            n = n / 10;
        }
        if (tong % 10 == 0)
            return true;
        return false;
    }

    private static boolean isThuanNghich(int n) {

        StringBuilder str = new StringBuilder();
        str.append(n);
        String checkThuanNghich = str.reverse().toString();
        String covertStr = String.valueOf(n);
        if (covertStr.equals(checkThuanNghich)) return true;
        else return false;
    }
}
