package com.wall.student.haquyduc;

import java.util.Scanner;

public class bai7muc2 {
    //    Tìm phần tử lớn nhất và lớn thứ 2 trong mảng cùng chỉ số của các số đó.
    static int[] arr;
    public static Scanner sc = new Scanner(System.in);

    public static void nhapMang() {
        System.out.print("Nhập n: ");
        int n = sc.nextInt();
        arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Nhập phần tử thứ " + i + " : ");
            arr[i] = sc.nextInt();
        }
//        int x;
//        System.out.print("Nhập phần tử cần chèn x =");
//        x=sc.nextInt();
//        //chen(arr,x);
    }



    public static void timMax() {
        int max = arr[0];
        int max2 = 0;
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (max == arr[i]) {
                continue;
            } else if (max2 < arr[i]) {
                max2 = arr[i];
            }
        }
        System.out.println("Phần tử lớn nhất là: " + max);
        System.out.println("Phần tử lớn thứ 2 là: " + max2);
    }

    public static void sapXep() {
        int temp = arr[0];
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
            System.out.print(arr[i] + " ");
        }
    }

    public static void main(String[] args) {
        nhapMang();
        timMax();
        sapXep();

    }
}
