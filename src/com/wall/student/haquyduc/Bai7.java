package com.wall.student.haquyduc;

public class Bai7 {
    public static void main(String[] args) {
//        int n = 0;
//        for (n = 10000; n <= 9999999; n++) {
//            if (isThuanNghich(n) && isSNT(n)) {
//                System.out.print(n + "; ");
//            }
//        }
        for (int i = 10000; i < 9999999; i++) {
            if (isSNT(i)) {
                for (int n = 1; n < 35; n++) {
                    if (tongchuso(i) == fibonaci(n)) {
                        System.out.println("thoa man: " + i);
                    }
                }
            }
        }
    }

    public static boolean isThuanNghich(int n) {
        StringBuilder str = new StringBuilder();
        str.append(n);
        String checkThuanNghich = str.reverse().toString();
        String covertStr = String.valueOf(n);
        if (covertStr.equals(checkThuanNghich)) return true;
        else return false;
    }

    public static boolean isSNT(int n) {
        if (n < 2) return false;
        int canBacHai = (int) Math.sqrt(n);
        for (int i = 2; i <= canBacHai; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }


    public static int fibonaci(int n) {
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        }
        return fibonaci(n - 1) + fibonaci(n - 2);
    }

    public static int tongchuso(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }
}

