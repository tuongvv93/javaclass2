package com.wall.student.haquyduc;

import java.util.Scanner;

public class Bai3 {
    public static void main(String[] args) {
        System.out.print("Nhập vào số nguyên: ");
        int n = CheckInput();
        PTSNT(n);
    }
    public static int CheckInput(){
        int n=0;
        boolean check=false;
        Scanner sc = new Scanner(System.in);
        while (!check){
            try{
                n=sc.nextInt();
                check=true;
            }catch (Exception e){
                System.out.println("Nhập lại:");
                sc.nextLine();
            }
        }
        return n;
    }
    public static boolean SNT(int a){
        if(a<2){
            return false;
        }
        for(int i=2;i<=Math.sqrt(a);i++){
            if(a%i==0) {
                return false;
            }
        }
        return true;
    }
    public static void PTSNT(int n){
        String result= "";
        System.out.print("Số đk phân tích là:");
        for(int i=2;i<=n;i++){
            while (SNT(i) && n%i==0){
                result=result+i+"x";
                n=n/i;
            }
        }
        result = result.substring(0, result.length()- 1);
        System.out.print(result);
    }
}
