package com.wall.student.haquyduc;

import java.util.Scanner;

public class Bai4 {
    public static void main(String[] args) {
        System.out.print("Nhập n = ");
        int n = CheckInput();
        System.out.printf("%d số nguyên tố đầu tiên là: ", n);
        int dem = 0;
        int i = 2;
        while (dem < n) {
            if (SNT(i)) {
                System.out.print(i + " ");
                dem++;
            }
            i++;
        }
    }

    public static int CheckInput() {
        int n = 0;
        boolean check = false;
        Scanner scanner = new Scanner(System.in);
        while (!check) {
            try {
                n = scanner.nextInt();
                check = true;
            } catch (Exception e) {
                System.out.println("Xin mời nhập vào số nguyên");
                scanner.nextLine();
            }
        }
        return n;
    }

    public static boolean SNT(int n) {
        if (n < 2) {
            return false;
        }
        int cbh = (int) Math.sqrt(n);
        for (int i = 2; i <= cbh; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
