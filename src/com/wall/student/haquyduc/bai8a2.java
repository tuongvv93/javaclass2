
package com.wall.student.haquyduc;

public class bai8a2 {
    static int[] a = new int[]{2, 3, 5, 7};
    static int[] so7ChuSo = new int[7];
    static int n = 0;

    public static void main(String[] args) {
        deQuy(0);
    }

    static void deQuy(int vt) {
        if (vt == 7) {
            int number = convertArrToNumber(so7ChuSo);
            if (isNguyenTo(number)) {
                System.out.println(number);
            }
            return;
        }
        for (int i = n; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                so7ChuSo[vt] = a[j];
                deQuy(vt + 1);
            }
        }
    }

    static int convertArrToNumber(int[] so7ChuSo) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            s = s.append(so7ChuSo[i]);
        }
        return Integer.parseInt(s.toString());
    }

    public static boolean isNguyenTo(int n) {
        if (n < 2) return false;
        if (n == 2) return true;
        int canBacHai = (int) Math.sqrt(n);
        for (int i = 3; i <= canBacHai; i += 2) {
            if (n % i == 0) return false;
        }
        return true;
    }

}
