package com.wall.student.haquyduc;

import java.util.Scanner;

public class Bai2 {
    public static void main(String[] args) {
        System.out.print("Nhập a: ");
        int a = checkInput();
        System.out.print("Nhập b:");
        int b = checkInput();
        System.out.println("UCLN của a và b la :" + UCLN(a, b));
    }

    public static int checkInput() {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        boolean check = false;
        while (!check) {
            try {
                n = sc.nextInt();
                check = true;
            } catch (Exception e) {
                System.out.println("Xin mời nhập lại:");
                sc.nextLine();
            }
        }
        return n;
    }

    public static int UCLN(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return UCLN(b, a % b);
        }
    }
}
