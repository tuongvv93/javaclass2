package com.wall.student.hung;

public class BaiTap8 {
    public static boolean isNguyenTo(long n) {
        double d = Math.sqrt(n);
        if (n > 1) {
            for (int i = 2; i <= d; i++) {
                if (n % i == 0) return false;
            }
            return true;
        } else return false;
    }

    public static boolean isDayNT(long n) {
        while (n != 0) {
            if (!isNguyenTo(n % 10))
                return false;
            n /= 10;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("các số nguyên có từ 5 đến 7 thoả mãn là :");
        for (long i = 2222223; i < 7777777; i++) {
            if (isNguyenTo(i) && isDayNT(i)) {
                System.out.print(i + " ");
            }
        }
    }
}
