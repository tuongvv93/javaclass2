package com.wall.student.hung;

import java.util.Scanner;

public class HungDo {
    private static Scanner scanner = new Scanner(System.in);

    public static boolean soCP(int n) {
        double a = Math.sqrt(n);
        int x = (int) a;
        if (x - a == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.print("Nhập số:");
        int n = scanner.nextInt();
        if (soCP(n) == true) {
            System.out.println("Là số chính phương");
        } else
            System.out.println("Không là số chính phương");
    }

}
