package com.wall.student.hung;
import java.util.Scanner;

public class BaiCTroChoiDoanSo {
    static void timKQ(int s, int d) {
        int a = 0;
        int b = 0;
        if (s > d && s > 0 && d > 0) {
            a = (s + d) / 2;
            b = (s - d) / 2;
            System.out.println("số a là :" + a);
            System.out.println("Số b là :" + b);
        } else
            System.out.println("Impossible");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số bộ test");
        int soBoTest = sc.nextInt();
        for (int i = 0; i < soBoTest; i++) {
            int s = sc.nextInt();
            int d = sc.nextInt();
            timKQ(s, d);
        }
    }
}
