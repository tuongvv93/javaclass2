package com.wall.student.hung;

import java.util.Scanner;

public class SoDep {
    public static void main(String[] args) {
        System.out.println("Nhập số đẹp:");
        int n = (new Scanner(System.in)).nextInt();
        if(isThuanNghich(n) && sum10(n)) {
            System.out.println("Là số đẹp");
        }
        else {
            System.out.println("Không là số đẹp");
        }
    }
    static boolean isThuanNghich(int n) {
        String numberStr = String.valueOf(n);
        int size = numberStr.length();
        for (int i = 0; i < (size / 2); i++) {
            if (numberStr.charAt(i) != numberStr.charAt(size - i - 1)) {
                return false;
            }
        }
        return true;
    }
    static boolean sum10(int n) {
        int dem = 0;
        while (n != 0) {
            dem += n % 10;
            n /= 10;
        }
        for (int i = 0; i <= dem; i++) {
            if (dem % 10 == 0) {
                return true;
            }
        }
        return false;
    }
}
