package com.wall.student.hung;

import java.util.Scanner;

public class BaiEDemSoLonHon {
    static void checkNumber(int[] arr) {
        int dem = 0;
        for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < i; j++) {
                    if(i == arr.length) break;
                    if (arr[i] < arr[j]) {
                        dem++;
                        i++;
                        j=0;
                    }
                }
        }
        int soKQ = arr.length - dem;
        System.out.println(soKQ);
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int soBoTest = sc.nextInt();
        for (int i = 0; i < soBoTest; i++) {
            int n = sc.nextInt();
            int[] arr = new int[n];
            for (int j = 0; j < n; j++) {
                arr[j] = sc.nextInt();
            }
            checkNumber(arr);
        }
    }
}
