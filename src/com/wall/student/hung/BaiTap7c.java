//package com.wall.student.hung;
//
//import java.util.Scanner;
//
//public class BaiTap7 {
//    static boolean isNguyenTo(int n) {
//        if (n <= 1) {
//            return false;
//        }
//        if (n == 2) {
//            return true;
//        }
//        for (int i = 3; i <= n / 2; i += 2) {
//            if (n % i == 0)
//                return false;
//        }
//        return true;
//    }
//    static boolean isThuanNghich(int n) {
//        String numberStr = String.valueOf(n);
//        int size = numberStr.length();
//        for (int i = 0; i < (size / 2); i++) {
//            if (numberStr.charAt(i) != numberStr.charAt(size - i - 1)) {
//                return false;
//            }
//        }
//        return true;
//    }
//    static int fibonacci(int n) {
//        if (n < 0) {
//            return -1;
//        } else if (n == 0 || n == 1) {
//            return n;
//        } else {
//            return fibonacci(n - 1) + fibonacci(n - 2);
//        }
//    }
//    static boolean checkFibo(int n) {
//        int dem = 0;
//        while (n != 0) {
//            dem += n % 10;
//            n /= 10;
//        }
//        for (int i = 0; i <= dem; i++) {
//            if (dem == fibonacci(i)) {
//                return true;
//            }
//        }
//        return false;
//    }
//    public static void main(String[] args) {
//        System.out.println("các số nguyên có từ 5 đến 7 thoả mãn là :");
//        for (int i = 10000; i < 9999999; i++) {
//            if (isNguyenTo(i) && isThuanNghich(i) && checkFibo(i)) {
//                System.out.print(i + " ");
//            }
//        }
//    }
//}
