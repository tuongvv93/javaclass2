package com.wall.student.hung;

import java.math.BigInteger;
import java.util.Scanner;

public class BaiF {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int soBoTest = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < soBoTest; i++) {
            BigInteger numberA = new BigInteger(sc.nextLine());
            BigInteger numberB = new BigInteger(sc.nextLine());
            BigInteger addAB = numberA.add(numberB);
            System.out.println(addAB);
        }
    }
}
