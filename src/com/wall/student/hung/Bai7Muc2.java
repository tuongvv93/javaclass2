package com.wall.student.hung;

import java.util.Scanner;

public class Bai7Muc2 {
    static int nhap() {
        Scanner input = new Scanner(System.in);
        boolean check = false;
        int n = 0;
        while (!check) {
            try {
                n = input.nextInt();
                if (n > 0 && n < 100) {
                    check = true;
                } else {
                    System.out.println("bạn phải nhập lại:");
                    input.nextLine();
                }
            } catch (Exception e) {
                System.out.println("bạn phải nhập lại:");
                input.nextLine();
            }
        }
        return (n);
    }

    static void printArray(int[] a, int begin, int end) {
        for (int i = begin; i < end; i++) {
            System.out.print(" " + a[i]);
        }
        System.out.println();
    }

    static void searchMax(int[] a) {
        int size = a.length, max2 = 0, max = a[0], key1 = 0, key2 = 0;
        for (int i = 0; i < size; i++) {
            if (a[i] > max) {
                max = a[i];
                key1 = i;
            }
        }
        for (int i = 0; i < size; i++) {
            if (a[i] < max && a[i] > max2) {
                max2 = a[i];
                key2 = i;
            }
        }
        System.out.println("Giá trị lớn nhất của mảng là :a[" + key1 + "] = " + max);
        System.out.println("Giá trị lớn thứ 2 của mảng là :a[" + key2 + "] = " + max2);
    }

    static void arrDesc(int[] a) {
        int temp = a[0];
        int size = a.length;
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] < a[j]) {
                    temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
        }
    }

    static void addNumber(int[] a, int n, int pt) {
        a[n + 1] = pt;
        arrDesc(a);
    }

    public static void main(String[] args) {
        System.out.print("Nhap n= ");
        int n = nhap();
        int[] a = new int[n + 2];
        int i;
        for (i = 0; i < n; i++) {
            System.out.print("\nNhập phần tử thứ [" + i + "] = ");
            a[i] = nhap();
        }
        searchMax(a);
        arrDesc(a);
        System.out.println("Mảng sau khi sắp xếp là");
        printArray(a, 0, n);
        System.out.println("Nhập phần từ muốn thêm vào:");
        int pt = nhap();
        addNumber(a, n, pt);
        printArray(a, 0, n + 1);
    }
}
