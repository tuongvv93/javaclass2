package com.wall.student.hung.io;

import java.io.*;

public class DocFile {
    public void readFile() {
        File file = new File("a.txt");
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String text = bufferedReader.readLine();
            while (text != null){
                System.out.println(text);
                text = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
