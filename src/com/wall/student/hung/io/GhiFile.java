package com.wall.student.hung.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GhiFile {
    public void writeFile(){
        File file = new File("a.txt");
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))){
            bufferedWriter.write("3");
            bufferedWriter.newLine();
            bufferedWriter.write("5");
            bufferedWriter.newLine();
            bufferedWriter.write("3 4 2 3 4");
            bufferedWriter.newLine();
            bufferedWriter.write("1");
            bufferedWriter.newLine();
            bufferedWriter.write("1");
            bufferedWriter.newLine();
            bufferedWriter.write("2");
            bufferedWriter.newLine();
            bufferedWriter.write("1 2");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


}
