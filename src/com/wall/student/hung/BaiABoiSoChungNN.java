package com.wall.student.hung;

import java.util.Scanner;

public class BaiABoiSoChungNN {
    static void BCNN(long a , long b) {
        long max = 0;
        long bcMax = a * b;
        if (a > b) {
            max = a;
        }
        else max = b;
        for (long i = max; i < bcMax; i+=max) {
            if (i % a == 0 && i % b == 0) {
                System.out.println("Bội chung nhỏ nhất là: "+i);
                break;
            }

        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int soBoTest = sc.nextInt();
        for (int i = 0; i < soBoTest; i++) {
            long a = sc.nextInt();
            long b = sc.nextInt();
            BCNN(a,b);
        }
    }
}
