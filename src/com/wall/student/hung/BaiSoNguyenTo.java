package com.wall.student.hung;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class BaiSoNguyenTo {
    static int[] b = new int[999999];
    static int n = 0;
    static boolean isNguyenTo(long n) {
        double d = Math.sqrt(n);
        if (n > 1) {
            for (int i = 2; i <= d; i++) {
                if (n % i == 0) return false;
            }
            return true;
        } else return false;
    }
    static boolean checkB(int a) {
        for(int i=0;i < n; i++) {
            if(b[i] == a) return false;
        }
        return true;
    }
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("file/a.txt"));
        while(true) {
            try{
                int a = sc.nextInt();
                System.out.println(a);
                if(isNguyenTo(a)) {
                    if (b[a] == 0)  {
                        n++;
                        b[a] = 1;
                    }
                }
            }
            catch (NoSuchElementException a) {
                break;
            }
        }
        System.out.println("Số kết quả là :");
        System.out.println(n);
    }

}
