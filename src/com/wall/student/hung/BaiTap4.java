package com.wall.student.hung;

import java.util.Scanner;

public class BaiTap4 {
    public static boolean soNguyenTo(int n) {
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số n: ");
        int n = scanner.nextInt();
        scanner.close();
        int dem = 0;
        int i = 2;
        System.out.println(n + " số nguyên tố đầu tiên là:");
        while (dem < n) {
            if (soNguyenTo(i)) {
                System.out.print(i + " ");
                dem++;
            }
            i++;
        }
    }
}
