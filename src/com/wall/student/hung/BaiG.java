package com.wall.student.hung;

import java.util.Scanner;

public class BaiG {
    int n;
    int[][] a = new int[1000][1000];
    int[][] b = new int[1000][1000];
    void nhapBoTest(Scanner sc) {
        StringBuilder s = new StringBuilder();
        int n = sc.nextInt();
        int dem = 0;
        for (int i = 0; i < n; i++) {
            a[i] = new int[n];
            for (int j = 0; j < n; j++) {
                a[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            b[i] = new int[n];
            for (int j = 0; j < n; j++) {
                b[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == b[i][j]) {
                    dem++;
                    s = s.append(i+" "+j+"\n");
                }
            }
        }
        System.out.println(dem);
        System.out.println(s);
    }
    public static void main(String[] args) {
        BaiG baiG = new BaiG();
        Scanner sc = new Scanner(System.in);
        int soBoTest = sc.nextInt();
        for (int f = 0; f < soBoTest; f++) {
            baiG.nhapBoTest(sc);
        }
    }
}
