package com.wall.student.ngovanhuy;


import java.util.Scanner;

public class BaiTap2 {
    public static int uocChungLonNhat(int a, int b) {
        if (b == 0) {
            return a;
        }
        return uocChungLonNhat(b, a % b);
    }

    public static void main(String[] args) {
        int a, b;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số a: ");
        a = sc.nextInt();
        System.out.print("Nhập số b: ");
        b = sc.nextInt();
        System.out.println("Ước chung lớn nhất " + a + "và" + b + "là :" + uocChungLonNhat(a, b));
    }
}
