package com.wall.student.ngovanhuy;


import java.util.Scanner;

public class BaiTap1 {
    public static boolean checkSNT(int n) {
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập n: ");
        int n = sc.nextInt();
        if (checkSNT(n)) {
            System.out.println(n + "Là số nguyên tố");
        } else {
            System.out.println(n + "Không phải là số nguyên tố");
        }
    }
}
