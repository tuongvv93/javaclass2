package com.wall.java1.ngovanhuy.bail;

public class SinhVien {
    private int maTS;
    private String tenTS;
    private String ngaySinh;
    private double mon1, mon2, mon3;

    @Override
    public String toString() {
        return "SinhVien{" +
                "maTS=" + maTS +
                ", tenTS='" + tenTS + '\'' +
                ", ngaySinh='" + ngaySinh + '\'' +
                ", mon1=" + mon1 +
                ", mon2=" + mon2 +
                ", mon3=" + mon3 +
                '}';
    }

    public int getMaTS() {
        return maTS;
    }

    public void setMaTS(int maTS) {
        this.maTS = maTS;
    }

    public String getTenTS() {
        return tenTS;
    }

    public void setTenTS(String tenTS) {
        this.tenTS = tenTS;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public double getMon1() {
        return mon1;
    }

    public void setMon1(double mon1) {
        this.mon1 = mon1;
    }

    public double getMon2() {
        return mon2;
    }

    public void setMon2(double mon2) {
        this.mon2 = mon2;
    }

    public double getMon3() {
        return mon3;
    }

    public void setMon3(double mon3) {
        this.mon3 = mon3;
    }

    public SinhVien(int maTS, String tenTS, String ngaySinh, double mon1, double mon2, double mon3) {
        this.maTS = maTS;
        this.tenTS = tenTS;
        this.ngaySinh = ngaySinh;
        this.mon1 = mon1;
        this.mon2 = mon2;
        this.mon3 = mon3;
    }

    public SinhVien() {
    }

    public double tinhDiemTrungBinh() {
        return this.mon1 + this.mon2 + this.mon3;
    }

    public void hienThi() {
        System.out.println("Mã thí sinh là: " + maTS);
        System.out.println("Tên thí sinh là: " + tenTS);
        System.out.println("Ngày sinh là: " + ngaySinh);
        System.out.println("Điểm môn 1 là: " + mon1);
        System.out.println("Điểm môn 2 là: " + mon2);
        System.out.println("Điểm môn 3 là: " + mon3);
        System.out.println("Điểm trung bình 3 môn là: " + tinhDiemTrungBinh());
        System.out.println("------------------------");
    }
}
