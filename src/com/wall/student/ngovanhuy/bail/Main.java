package com.wall.java1.ngovanhuy.bail;

import java.util.Scanner;

public class Main {
    static Scanner sc = new Scanner(System.in);

    static void nhapThongTin(SinhVien sv) {
        System.out.println("Nhập mã thí sinh: ");
        sv.setMaTS(sc.nextInt());
        sc.nextLine();
        System.out.println("Nhập tên thí sinh: ");
        sv.setTenTS(sc.nextLine());
        System.out.println("Nhập ngày sinh: ");
        sv.setNgaySinh(sc.nextLine());
        System.out.println("Nhập điểm môn 1: ");
        sv.setMon1(sc.nextDouble());
        System.out.println("Nhập điểm môn 2: ");
        sv.setMon2(sc.nextDouble());
        System.out.println("Nhập điểm môn 3: ");
        sv.setMon3(sc.nextDouble());
    }

    public static void main(String[] args) {
        SinhVien ts[] = null;
        int a, n = 0;
        boolean flag = true;
        do {
            System.out.println("Bạn chọn làm gì?");
            System.out.println("1.Nhập thông tin thí sinh.\n" +
                    "2.Xuất bản danh sách thí sinh.\n" +
                    "Nhập số khác để thoát");
            a = sc.nextInt();
            switch (a) {
                case 1:
                    System.out.println("Nhập số lượng n: ");
                    n = sc.nextInt();
                    ts = new SinhVien[n];
                    for (int i = 0; i < n; i++) {
                        System.out.println("Thí sinh thứ " + (i + 1) + ": ");
                        ts[i] = new SinhVien();
                        nhapThongTin(ts[i]);
                    }
                case 2:
                    SinhVien temp = ts[0];
                    for (int i = 0; i < ts.length - 1; i++) {
                        for (int j = i + 1; j < ts.length; j++) {
                            if (ts[i].tinhDiemTrungBinh() < ts[j].tinhDiemTrungBinh()) {
                                temp = ts[i];
                                ts[i] = ts[j];
                                ts[j] = temp;
                            }
                        }
                    }
                    for (int i = 0; i < n; i++) {
                        ts[i].hienThi();
                    }
                    break;
                default:
                    System.out.println("Đóng");
                    flag = false;
                    break;
            }
        } while (flag);
    }
}
