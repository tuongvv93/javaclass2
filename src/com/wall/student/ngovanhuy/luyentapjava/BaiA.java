package com.wall.student.ngovanhuy.luyentapjava;
import java.util.Scanner;

public class BaiA {
    public static void BCNN(int a, int b) {
        int i;
        i = (a > b) ? a : b;
        while (true) {
            if (i % a == 0 && i % b == 0) {
                System.out.printf("Bội chung nhỏ nhất là %d", i);
                break;
            }
            ++i;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int boTest = sc.nextInt();
        for (int i = 0; i < boTest; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            BCNN(a, b);
        }
    }
}
