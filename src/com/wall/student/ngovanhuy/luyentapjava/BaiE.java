package com.wall.student.ngovanhuy.luyentapjava;

import java.util.Scanner;

public class BaiE {
    public static void demSoLonHon(int[] a) {
        int dem = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < i; j++) {
                if (i == a.length) {
                    break;
                }
                if (a[i] < a[j]) {
                    dem++;
                    i++;
                    j = 0;
                }
            }
        }
        int ketQua = a.length - dem;
        System.out.println(ketQua);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int boTest = sc.nextInt();
        for (int i = 0; i < boTest; i++) {
            int n = sc.nextInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = sc.nextInt();
            }
            demSoLonHon(a);
        }
    }
}
