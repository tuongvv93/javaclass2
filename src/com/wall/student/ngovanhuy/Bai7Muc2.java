package com.wall.student.ngovanhuy;

import java.util.Scanner;

public class Bai7Muc2 {
    public static void main(String[] args) {
        int n, x;
        int a[] = new int[100];
        Scanner in = new Scanner(System.in);
        System.out.print("Nhập n: ");
        n = in.nextInt();
        System.out.println("Nhập các phần tử mảng: ");
        for (int i = 1; i <= n; i++) {
            System.out.print("a[" + i + "]= ");
            a[i] = in.nextInt();
        }
        timMax(a, n);
        giamDan(a, n);
        System.out.println("day sau khi sap xep giam dan la: ");
        for (int i = 1; i <= n; i++) {
            System.out.print(" " + a[i]);
        }
        System.out.print("\nNhập số cần chèn: ");
        x = in.nextInt();
        chenSo(a, x, n);
    }

    public static void timMax(int[] a, int n) {
        int max1 = 0;
        int max2 = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > max1) {
                max2 = max1;
                max1 = a[i];
            } else if (a[i] > max2) {
                max2 = a[i];
            }
        }
        System.out.println("Phần tử lớn thứ nhất là: " + max1);
        System.out.println("Phần tử lớn thứ hai là: " + max2);
    }

    public static void giamDan(int[] a, int n) {
        int tg;
        for (int i = 1; i < n; i++) {
            for (int j = i + 1; j <= n; j++) {
                if (a[i] < a[j]) {
                    tg = a[i];
                    a[i] = a[j];
                    a[j] = tg;
                }
            }
        }
    }

    public static void chenSo(int[] a, int x, int n) {
        n++;
        a[n] = x;
        giamDan(a, n);
        System.out.println("Dãy sau khi chèn là: ");
        for (int i = 1; i <= n; i++) {
            System.out.print(" " + a[i]);
        }
    }

}
