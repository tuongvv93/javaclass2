package com.wall.student.ngovanhuy;

import java.util.Scanner;

public class BaiTap4 {
    public static boolean checkSNT(int n) {
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập n: ");
        n = sc.nextInt();

        int dem = 0;
        int x = 2;
        while (true) {
            if (checkSNT(x)) {
                System.out.print(x + " ");
                dem++;
            }
            if (dem == n) break;
            if (x == 2) {
                x++;
            } else {
                x += 2;
            }
        }
    }
}
