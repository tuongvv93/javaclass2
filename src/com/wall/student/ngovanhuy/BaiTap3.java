package com.wall.student.ngovanhuy;

import java.util.Scanner;

public class BaiTap3 {
    public static void phanTich(int n) {
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                n = n / i;
                if (n == 1) {
                    System.out.print(i);
                } else {
                    System.out.print(i + "x");
                }
            }
            if (n == 1) break;
        }
    }

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập một số nguyên: ");
        n = sc.nextInt();
        phanTich(n);
    }
}

