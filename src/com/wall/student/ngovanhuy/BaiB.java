package com.wall.student.ngovanhuy;

import java.util.Scanner;

public class BaiB {
    public static boolean thuanNgich(int n) {
        int m = n;
        int dao = 0;
        while (n > 0) {
            dao = dao * 10 + n % 10;
            n /= 10;
        }
        return dao == m;
    }

    public static boolean tongChia10(int n) {
        int tong = 0;
        while (n > 0) {
            tong = tong + n % 10;
            n /= 10;
        }
        if (tong % 10 == 0) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int dem = 0;
        for (int i = 0; i <= n; i++) {
            if (thuanNgich(i) && tongChia10(i)) {
                dem++;
            }
        }
        System.out.print(dem);
    }

}
