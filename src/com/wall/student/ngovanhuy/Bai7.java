package com.wall.student.ngovanhuy;

public class Bai7 {
    public static int fiBo(int n) {
        if (n < 0) {
            return -1;
        } else if (n == 0 || n == 1) {
            return n;
        } else {
            return fiBo(n - 1) + fiBo(n - 2);
        }
    }

    public static boolean ktFiBo(int n) {
        for (int i = 1; i <= 10; i++) {
            if (n == fiBo(i)) {
                return true;
            }
        }
        return false;
    }

    public static int tongFiBo(int n) {
        int s = 0;
        while (n > 0) {
            s = s + n % 10;
            n = n / 10;
        }
        return s;
    }

    public static void main(String[] args) {
        for (long i = 1000000; i <= 999999999; i++) {
            if (ktFiBo(tongFiBo((int) i))) {
                System.out.print(i + " ");
            }
        }
    }
}
