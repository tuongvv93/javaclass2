package com.wall.java1.baitap1;

import java.util.Scanner;

public class BaiH {


    public static void main(String[] args) {
        BaiH h = new BaiH();
        h.nhap();
    }

    private void nhap() {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < n; i++) {
            String s = sc.nextLine();
            String ketQua = xuLy(s);
            System.out.println(ketQua);
        }
    }

    private String xuLy(String a) {
        //22
        // i = 0
        // soLapLai = 1
        StringBuilder ketQua = new StringBuilder("");
        for (int i = 0; i < a.length(); i++) {
            int soLapLai = 1;
            while (soLapLai < a.length() - i && a.charAt(i) == a.charAt(i + soLapLai)) {
                soLapLai++;
            }
            ketQua = ketQua.append(soLapLai +""+ a.charAt(i));
            i += soLapLai;
        }
        return ketQua.toString();
    }
}
