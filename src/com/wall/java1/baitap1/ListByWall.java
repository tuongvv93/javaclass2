package com.wall.java1.baitap1;

public class ListByWall<T> {
    T[] datas;

    public ListByWall() {
    }

    public T[] getDatas() {
        return datas;
    }

    public void setDatas(T[] datas) {
        this.datas = datas;
    }
}
