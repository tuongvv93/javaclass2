package com.wall.java1.baitap1;


import java.util.*;

public class Bai7 {

    public static void main(String[] args) {
        a();
        b();
        c();
    }
    static void c() {
        Integer[] a = new Integer[]{5, 4, 32, 15, 6, 3, 5, 2, 4, 0};

    }
    static void b() {
        Integer[] a = new Integer[]{5, 4, 32, 15, 6, 3, 5, 2, 4, 0};
        // cach 1
        Arrays.sort(a, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        for (int i =0; i < a.length; i ++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();

        // cach 2
        List<Integer> list = new ArrayList<>();  // generic
        list.add(100);
        list.addAll(Arrays.asList(a));

        Collections.sort(list, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        // binary search
        System.out.println(list);
    }

    static void a() {
        int[] a = new int[]{5, 4, 32, 15, 6, 3, 5, 2, 4, 0};
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;

        for (int i = 0; i < a.length; i++) {
            if (a[i] > max1) {
                max1 = a[i];
            }
            if (a[i] > max2 && a[i] != max1) {
                max2 = a[i];
            }
        }
        System.out.println(max1 + " " + max2);
    }
}
