package com.wall.java1.baitap1;

import java.util.Scanner;

public class BaiSoDep {
    static int soChuSo, dem;
    static int[] chuSo = new int[11];

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        // cach 1
//        long[] min= new long[]{0, 0, 100, 1000, 10000      , 100000, 1000000};
//        long[] max= new long[]{0, 10, 1000, 10000, 100000, 1000000, 10000000};
//        for (int i =0; i < n ; i ++) {
//            soChuSo = sc.nextInt();
//            // soChu so = 3
//            int dem = 0;
//            for (long j = min[soChuSo]; j < max[soChuSo]; j ++) {
//                if (thuanNghich(j) && tongChuSoChiaHet(j)) {
//                    dem++;
//                }
//            }
//            System.out.println(dem);
//        }


        // cach 2
//        long[] ketQua= new long[]{0, 0, 1, 3, 4, 90, 6, 7, 8,9,10};
//        for (int i =0; i < n ; i ++) {
//            soChuSo = sc.nextInt();
//            System.out.println(ketQua[soChuSo]);
//        }

        // cach 3
        //
        for (int i = 0; i < n; i++) {
            soChuSo = sc.nextInt();
            dem = 0;
            deQuy(0);
            System.out.println(dem);
        }

    }

    static void deQuy(int vt) {
        // truong hop cuoi cung
        if (soChuSo % 2 == 0) {
            if (vt == soChuSo / 2) {
                process(chuSo, vt, true);
                return;
            }
        } else {
            if (vt == soChuSo / 2 + 1) {
                process(chuSo, vt, false);
                return;
            }
        }


        int i = 0;
        if (vt == 0) {
            i = 1;
        }
        for (; i < 10; i++) {
            chuSo[vt] = i;
            deQuy(vt + 1);
        }
    }

    private static void process(int[] chuSo, int vt, boolean isSoChan) {
        //  xu ly tiep duoc khong ?
        int sum = 0;
        if (isSoChan) {
            for (int i = 0; i < vt; i++) {
                sum += chuSo[i];
            }
            if ((sum * 2) % 10 == 0) dem++;
            return;
        }
        for (int i = 0; i < vt - 1; i++) {
            sum += chuSo[i];
        }
        if (((sum * 2) + chuSo[vt - 1]) % 10 == 0) dem++;
    }

}
