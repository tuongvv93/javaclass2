package com.wall.java1.baitap1;

public class StringLearn {
    public static void main(String[] args) {
        String s = "AbC";
        String s2 = "321";
        System.out.println(s.compareTo(s2));
        for (int i = 0; i < s.length(); i++) {
            //  s.length() = 3
            // 0 1 2
            System.out.println(s.charAt(i));
        }
        System.out.println(s.toLowerCase());
        System.out.println(s.toUpperCase());
        System.out.println(s.isEmpty());
        System.out.println(s.substring(0, 2));
        System.out.println(s.substring(1));

        StringBuilder str = new StringBuilder(s);
        str = str.reverse();
        str = str.append("123");
        System.out.println(str);

        {
            int a = 0;
        }
    }
}
