package com.wall.java1.baitap1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

public class BaiF {
    public static void main(String[] args) {
        //alt + tab
        String s = new Scanner(System.in).nextLine();

        BigInteger numberA = new BigInteger(s);
        BigInteger numberB = new BigInteger("52347623547658462347863264736478364823646483264823648326486384622");
        BigDecimal decimalA = new BigDecimal("6.5458489367843577895464564");

        System.out.print(numberA.subtract(numberB) +" " + 9);
    }
}
