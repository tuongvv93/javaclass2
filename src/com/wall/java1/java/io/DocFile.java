package com.wall.java1.java.io;

import java.io.*;

public class DocFile {
    public void readFile() {
        File file = new File("a.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            int soBoTest = Integer.parseInt(bufferedReader.readLine());
            while (soBoTest-- > 0) {
                int n = Integer.parseInt(bufferedReader.readLine());
                String s = bufferedReader.readLine();
                String[] arr = s.split(" ");
                int sum = 0;
                for (int i = 0; i < arr.length; i++) {
                    int number = Integer.parseInt(arr[i]);
                    sum += number;
                }
                System.out.println(sum);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}