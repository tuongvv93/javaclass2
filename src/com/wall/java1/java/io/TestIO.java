package com.wall.java1.java.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class TestIO {

    public static void main(String[] args) {
        TestIO test = new TestIO();
        test.Read();
        test.Write();
    }

    void Read() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"))) {
            int t = bufferedReader.read() - '0';

            for (int i = 0; i < t; i++) {
                char x = (char) bufferedReader.read();
                if (x == 13) {
                    x = (char) bufferedReader.read();
                    x = (char) bufferedReader.read();
                }
                int n = x - '0';
                x = (char) bufferedReader.read();
                x = (char) bufferedReader.read();

                int sum = 0;
                for (int j = 0; j < n * 2 - 1; j++) {
                    int number = bufferedReader.read() - '0';
                    if (j %2 == 1) continue;
                    sum += number;
                }
                x = (char) bufferedReader.read();
                x = (char) bufferedReader.read();

                System.out.println(sum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void Write() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"))) {
            bufferedWriter.write("abc");
            bufferedWriter.newLine();
            bufferedWriter.write("abc2");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}