package com.wall.java1.java.io.henhiphan;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class WriterObjectFile {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<StudentWriteFile> list = new ArrayList<>();
        list.add(new StudentWriteFile());
        list.add(new StudentWriteFile());
        list.add(new StudentWriteFile());

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("file/object.dat"));
        objectOutputStream.writeObject(list);
        objectOutputStream.close();


        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("file/object.dat"));
        List<StudentWriteFile> list2 = (List<StudentWriteFile>) objectInputStream.readObject();
        for (StudentWriteFile student : list2) {
            System.out.println(student.getId() + " " + " " + student.getAge());
        }
        objectInputStream.close();
    }
}
