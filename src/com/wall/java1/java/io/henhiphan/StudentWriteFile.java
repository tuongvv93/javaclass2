package com.wall.java1.java.io.henhiphan;

import java.io.Serializable;

public class StudentWriteFile implements Serializable {
    private int id;
    private int age;
    private String name;
    private String name2;

    public StudentWriteFile() {
        id = 10;
        age = 8;
        name = "Student name 1";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
