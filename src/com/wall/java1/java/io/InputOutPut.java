package com.wall.java1.java.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class InputOutPut {
    // đọc vào 1 file có cấu trúc như sau:
    //3
    //5
    //3 4 2 3 4
    //1
    //1
    //2
    //1 2

    // xử lý file
    // đọc từ file
    public static void main(String[] args) {
        // ghi ra file
        // c1
        File file = new File("a.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write("abc");
            bufferedWriter.newLine();
            bufferedWriter.write("abc2");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //c2 DataOuPutStream

        // đọc
        //c1 Scanner  -> đọc ghi\

        try {
            Scanner scanner = new Scanner(file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

}
