package com.wall.java1.java;

public class Sach {
    private String tenSach;
    private NhaXuatBan nhaXuatBan;

    public Sach() {
    }

    public Sach(String tenSach, NhaXuatBan nhaXuatBan) {
        this.tenSach = tenSach;
        this.nhaXuatBan = nhaXuatBan;
    }

    public String getTenSach() {
        return tenSach;
    }

    public void setTenSach(String tenSach) {
        this.tenSach = tenSach;
    }

    public NhaXuatBan getNhaXuatBan() {
        return nhaXuatBan;
    }

    public void setNhaXuatBan(NhaXuatBan nhaXuatBan) {
        this.nhaXuatBan = nhaXuatBan;
    }
}
