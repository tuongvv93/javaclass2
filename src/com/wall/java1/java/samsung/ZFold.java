package com.wall.java1.java.samsung;

public class ZFold extends DienThoai {
    private boolean gapDoc;

    public boolean isGapDoc() {
        return gapDoc;
    }

    public void setGapDoc(boolean gapDoc) {
        this.gapDoc = gapDoc;
    }

    @Override
    public void xuatDuLieu() {
        System.out.println(isNghe() + " " + gapDoc);
    }

    @Override
    public boolean ngheMay() {
        System.out.println(" mở dọc");
        return true;
    }
}
