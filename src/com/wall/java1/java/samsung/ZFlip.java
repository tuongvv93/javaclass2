package com.wall.java1.java.samsung;

public class ZFlip extends DienThoai {
    private boolean gapNgang;

    public boolean isGapNgang() {
        return gapNgang;
    }

    public void setGapNgang(boolean gapNgang) {
        this.gapNgang = gapNgang;
    }

    @Override
    public boolean ngheMay() {
        System.out.println(" mở ngang");
        return true;
    }
}
