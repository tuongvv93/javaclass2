package com.wall.java1.java.samsung;

public abstract class DienThoai {
    private boolean nghe;
    private boolean goi;

    public void xuatDuLieu() {
        System.out.println(" nghe: "+ nghe );
    }

    public void traLoi() {
        if (ngheMay()) {
            System.out.println("Có trả lời");
            return;
        }
        System.out.println("Không trả lời");
    }

    public abstract boolean ngheMay();

    public boolean isNghe() {
        return nghe;
    }

    public void setNghe(boolean nghe) {
        this.nghe = nghe;
    }

    public boolean isGoi() {
        return goi;
    }

    public void setGoi(boolean goi) {
        this.goi = goi;
    }
}
