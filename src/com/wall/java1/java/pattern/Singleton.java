package com.wall.java1.java.pattern;

import java.util.Scanner;

public class Singleton {
    private static Singleton mInstances;

    // khởi tạo duy nhất 1 lần
    public static Singleton getInstances() {
        if (mInstances == null) mInstances = new Singleton();
        return mInstances;
    }

    public void nhapDuLieu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Moi ban nhap");
        System.out.println(scanner.nextLine());
    }
}
