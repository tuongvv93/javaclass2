package com.wall.java1.java.pattern;

public class Student3 {
    private int age;
    private String name;

    public Student3(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Student3Factory builder() {
        return new StudentTemp();
    }

    interface Student3Factory {
        Student3Factory setName(String name);

        Student3Factory setAge(int age);

        Student3 build();
    }

    static class StudentTemp implements Student3Factory {
        private int age;
        private String name;

        @Override
        public Student3Factory setName(String name) {
            this.name = name;
            return this;
        }

        @Override
        public Student3Factory setAge(int age) {
            this.age = age;
            return this;
        }

        @Override
        public Student3 build() {
            return new Student3(age, name);
        }
    }
}
