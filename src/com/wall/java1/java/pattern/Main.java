package com.wall.java1.java.pattern;

public class Main {
    void singleton() {
        // vi du singleton:
        // thay doi code duoi tot hon
        new Singleton().nhapDuLieu();
        new Singleton().nhapDuLieu();
        new Singleton().nhapDuLieu();

        // tiet kiem bo nho
        // tranh khoi tao nhieu lan
        Singleton.getInstances().nhapDuLieu();
        Singleton.getInstances().nhapDuLieu();
        Singleton.getInstances().nhapDuLieu();
    }

    public static void main(String[] args) {
        //new Main().singleton();


        // builder
        Student3 student = Student3.builder()
                .setAge(10)
                .setName("ABC")
                .build();
        System.out.println(student.getName() + " " + student.getAge());
    }
}
