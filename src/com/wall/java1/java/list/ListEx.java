package com.wall.java1.java.list;

import java.util.*;

public class ListEx {
    int mang[] = new int[1000000000];
    public static void main(String[] args) {
        // đơn luồng
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(11);
        list.add(12);
        System.out.println(list.size());
        System.out.println(list.get(1));

        // đa luồng => dạy buổi sau
        Vector<Integer> vector = new Vector<>();
        vector.add(10);
        vector.add(11);
        vector.add(12);
        System.out.println(vector.size());
        System.out.println(vector.get(1));

        // Map

        Map<String, Integer> mapIntByFloat = new HashMap<>();
        mapIntByFloat.put("xxxx", 6);
        System.out.println(mapIntByFloat.get("xxxx"));
        System.out.println(mapIntByFloat.containsKey("xxxx"));
        mapIntByFloat.remove("xxxx");
        System.out.println(mapIntByFloat.get("xxxx"));
    }
}
