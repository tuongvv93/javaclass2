package com.wall.java1.java;

public class SinhVien2 {
    private int maSinhVien;
    private int tuoi;
    private String hoVaTen;
    private int gioiTinh;

    public SinhVien2(int maSinhVien, int tuoi, String hoVaTen, int gioiTinh) {
        this.maSinhVien = maSinhVien;
        this.tuoi = tuoi;
        this.hoVaTen = hoVaTen;
        this.gioiTinh = gioiTinh;
    }

    public SinhVien2() {
        tuoi = 21;
    }

    public String getTen() {
        return hoVaTen;
    }

}
