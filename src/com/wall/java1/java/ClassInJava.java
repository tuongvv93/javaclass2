package com.wall.java1.java;

import com.wall.java1.java.interface1.LangNgheClick;
import com.wall.java1.java.interface1.NutBanPhim;
import com.wall.java1.java.samsung.DienThoai;
import com.wall.java1.java.samsung.ZFold;

public class ClassInJava {

    public static void main(String[] args) {
//        Sach sach = new Sach();
//        NhaXuatBan x = new NhaXuatBan();
//        sach.setNhaXuatBan(x);
//
//        BaiG baiG = new BaiG();
////        baiG. = 9;
//        ZFold zFold = new ZFold();
//        zFold.isGoi();


        DienThoai dienThoai = new ZFold();
        dienThoai.xuatDuLieu();
//        dienThoai.ngheMay();
//        dienThoai.traLoi();
//
//        NganHang nganHang = new VCB();
//        nganHang.rutTien();

        // interface => callback  =>
        NutBanPhim nutBanPhim = new NutBanPhim();
        boolean ok = true;

        nutBanPhim.getValue(new LangNgheClick() {
            @Override
            public boolean getFullData() {

                return ok;
            }
        });
    }

}
