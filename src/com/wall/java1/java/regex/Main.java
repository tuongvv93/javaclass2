package com.wall.java1.java.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String phone = "0987635452";
        String email = "698763545236";
        String other = "0987-6354-5234";

//        if ((phone.startsWith("0") || phone.startsWith("84") || phone.startsWith("0084")) && phone.length() == 10) {
//            System.out.println("đây là số điện thoại");
//        }
        // regex

        // email
        //[a-zA-Z0-9]{3,}@{1}[a-z]{2,}\.[a-z]{2,}
        // phone
        //^(0084{1})([0-9]{9})$|^(0{1})([0-9]{9})$|^(84{1})([0-9]{9})$
        Pattern pattern = Pattern.compile("^[0-9]");
        Matcher matcher = pattern.matcher(phone);
        //System.out.println(matcher.matches());

        if (matcher.find()) {
            System.out.println("OK");
        } else {
            System.out.println("NOT OK");
        }

    }
}
