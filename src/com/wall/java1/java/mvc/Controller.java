package com.wall.java1.java.mvc;

// tinh tong tu 1 den 10
public class Controller {
    View view;
    Model model;
    private static Controller mInstance;

    public static Controller getInstance() {
        if (mInstance == null) mInstance = new Controller();
        return mInstance;
    }

    public Controller() {
        view = new View();
        model = new Model();
    }

    public void nhapDuLieu() {
        view.printConsole("Mời bạn nhập số n:");
        model.setN(view.getInt());
    }

    public void xuLy() {
        int sum = 0;
        for (int i = 1; i < model.getN(); i++) {
            sum += i;
        }
        model.setSum(sum);
    }

    public void xuatDuLieu() {
        view.printConsole("kết quả:" + model.getSum());
    }

    public void start() {
        nhapDuLieu();
        xuLy();
        xuatDuLieu();
    }
}
