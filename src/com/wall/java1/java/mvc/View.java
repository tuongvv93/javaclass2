package com.wall.java1.java.mvc;

import java.util.Scanner;

public class View {
    Scanner scanner;

    public View() {
        scanner = new Scanner(System.in);
    }

    public void printConsole(String s) {
        System.out.println(s);
    }
    public int getInt() {
        return scanner.nextInt();
    }
}
