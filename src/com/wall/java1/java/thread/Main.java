package com.wall.java1.java.thread;

public class Main {
    static long mutil1 = 1;
    static long mutil2 = 1;

    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                thread1();
            }
        });
        t.start();
        System.out.println("Ket qua -------");

    }

    static void thread1() {
        for (long i =1; i < 20; i++) {
            mutil1 *= i;
            System.out.println("Ket qua: " + mutil1);
        }

    }

    static void thread2() {
        for (long i =100000; i < 200000; i++) {
            mutil2 *= i;
        }
    }
}
