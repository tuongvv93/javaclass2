package com.wall.java1;

import java.util.Scanner;

public class HelloWorld {

    public static void main(String[] args) {
        int á = 0;
        boolean b = false;
        float c = 6.0f;
        double d = 7d;
        char m = 'c';
        int[] mang = new int[100];
        mang[0] = 1009;
        char[] mangChar = new char[1000];
        String s = "hahahaha";

        System.out.println("HelloWorld!");
        // algorithm // for while if else  => math
        // kieu du lieu   => số nguyên: int   0 1 2 23 4 5  100 9999
        // => số thập phân:  float  0.5
        // đúng sai : bool a  = true;   = false;
        //  double  to hơn float chứa được nhiều hơn
        int a = (int) 6.7f;  // a = 6

        // kiểm tra một số có phải số chính phương hay không thì mình sẽ làm ntn?
        // số chính phương là số ntn?
        // 4   căn 4 = 2  số nguyên   25  16  căn của nó đều là số nguyên

        // nhập từ bàn phím:
        Scanner scanner = new Scanner(System.in);
        scanner.nextInt();
    }

}
